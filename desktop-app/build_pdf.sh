#!/bin/bash

sudo docker run --rm -it -v ${PWD}:/docs -v ${PWD}/../../assets:/docs/assets -v ${PWD}/../../overrides:/docs/overrides registry-gitlab-2-production.restr.im:5005/cctv-dotnet/build build
