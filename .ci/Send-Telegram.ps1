param(
    [string] $botToken,
    [Int64] $chatID,
    [string] $message
)

if ((Get-Command "Send-TelegramTextMessage" -errorAction SilentlyContinue)) {
  Send-TelegramTextMessage -BotToken $botToken -ChatID $chatID -Message $message
}