param ([string]$slackUriWebhook, [string]$branchName, [string]$messageText)

if (Get-Command "Send-SlackMessage") {
  Send-SlackMessage -Uri "$env:SLACK_BUILD_URI_WEBHOOK" -Text "$messageText"
}