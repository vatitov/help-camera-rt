# Скачивание дистрибутивов Настольного приложения и VMS

## Настольное приложение

Ссылки на скачивание дистрибутивов Настольного приложения:

* [Сборка для 64-битных систем](https://help.camera.rt.ru/files/desktop-b2b/Rt.B2b.VideoClient-latest-x64.msi)
* [Сборка для 32-битных систем](https://help.camera.rt.ru/files/desktop-b2b/Rt.B2b.VideoClient-latest-x86.msi)

[Ссылка на инструкцию по установке Настольного приложения](https://help.camera.rt.ru/desktop_app/installation/) 

## VMS

[Ссылка на скачивание дистрибутива](https://help.camera.rt.ru/files/vms/vms-installer.zip)

Ссылки на инструкции по установке VMS:

* [Онлайн-версия VMS](https://help.camera.rt.ru/vms/vms_install_online/)
* [Оффлайн-версия VMS](https://help.camera.rt.ru/vms/vms_install_offline/) — в этом случае скачивать дистрибутив не нужно, потому что установка происходит по другому алгоритму.