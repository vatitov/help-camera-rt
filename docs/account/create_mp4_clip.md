# Создание MP4-клипа
Клип — MP4-файл, который содержит отрывок из видео, записанного вашей камерой. Клип можно запросить, указав его временные границы и тип сжатия видео в нем, а потом загрузить на свой компьютер.
Клипы можно запрашивать только от своих камер. Если камеру вам предоставили по ссылке или по почте, вы не можете загрузить клип с ее видео. Количество и длительность клипов, которые вы можете запросить, регулируются тарифным планом вашей организации.

## Запрос клипа

1. Откройте страницу с видео от камеры.
1. Нажмите на кнопку в виде фрагмента кинематографической ленты в нижнем правом углу плеера.
1. Нажмите на появившуюся под таймлайном шестеренку, чтобы выбрать способ формирования клипа.
1. Отметьте нужную опцию.
1. Нажмите на дату рядом с шестеренкой, чтобы вызвать календарь.
1. Выберите дату, в которую записано видео, которое нужно выгрузить.
1. Введите время начала и конца клипа.
1. Нажмите «Сохранить».

После этого запрошенный вами клип появится в списке клипов, откуда вы сможете его скачать. Перейдите к списку клипов, нажав на изображение камеры рядом с профилем пользователя и выбрав пункт меню «Клипы».
