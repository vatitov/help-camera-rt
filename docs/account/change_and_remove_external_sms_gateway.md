# Изменение и удаление внешнего SMS-шлюза

## Изменение настроек внешнего шлюза

1. Откройте боковое меню.
1. Нажмите «Настройки шлюза» в разделе «SMS-уведомления».
1. Укажите нужные данные в новых полях.
1. Нажмите «Сохранить» внизу страницы.

## Удаление внешнего шлюза
Чтобы удалить шлюз, нужно стереть его настройки полностью или частично и сохранить изменения.
Если вам требуется стереть все данные шлюза, сотрите все его настройки и удалите дополнительные параметры, нажав на кнопки-минусы рядом с ними. После этого нажмите «Сохранить».
Если вам не нужно скрывать все данные шлюза из корпоративного аккаунта, а требуется запретить подключение к шлюзу, сотрите некоторые параметры — например, значения логина и пароля. Также сохраните изменения.
Возможно, удаление данных шлюза не требуется и достаточно переключиться на шлюз поставщика услуги.
