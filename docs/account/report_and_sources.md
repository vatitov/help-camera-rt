# Отчет и источники
Отчеты — это XLSX-файлы, которые содержат данные о работе вашей организации в системе. Вы можете самостоятельно формировать отчеты, включая в них нужные вам данные. Когда вы создаете отчет, его настройки сохраняются и вы всегда сможете загрузить новый отчет за указанное вами время.
Кнопка для создания нового отчета и список уже сформированных вами отчетов находятся в боковой панели в разделе «Отчеты».

## Источники
Источники — это SQL-запросы к базе данных системы, которые администратор создал в административном интерфейсе для вашей организации. Отчеты состоят из источников.
Например, источник может запрашивать у базы данных список всех камер, зарегистрированных указанными сотрудниками. Включите источник в отчет, и в итоговом загруженном файле один лист XLSX-таблицы будет посвящен списку камер, которые сотрудники зарегистрировали за указанное вами время. Для каких сотрудников нужен отчет по камерам, вы указываете при создании отчета или при его запросе; за какое время вам нужна статистика, также указывается при запросе файла отчета.
В отчет можно включить несколько источников. Источники в одном отчете могут быть любыми — выдача по каждому из них займет свой лист в итоговом файла.

Если при создании отчета вы не находите нужного источника:

1. попросите ваших администраторов создать его, если у вашей организации есть доступ к административному интерфейсу;
2. обратитесь к менеджеру Ростелекома, если доступа к административному интерфейсу системы у вашей организации нет.
